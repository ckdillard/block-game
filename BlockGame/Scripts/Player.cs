using Godot;
using System;

public class Player : KinematicBody
{
    Spatial head;
    Camera camera;
    
    private float _cameraAngle = 0f, _gravity = -9.8f;

    private int _flySpeed = 20, _flyAccel = 4;
    private int _upwardSpeed = 20;
    private bool _isFlying = true;

    public bool IsFlying { get => _isFlying; }    
    public float MouseSensitivity { get; private set; }
    public float VerticalMomentum { get; private set; }
    public Vector3 Velocity { get; private set; }
    public Vector3 Direction { get; private set; }

    public override void _Ready()
    {
        // Movement of camera.
        head = (Spatial)GetNode("Head");
        camera = (Camera)GetNode("Head/Camera");
        MouseSensitivity = 0.3f;
        
        // Movement of character.
        Velocity = new Vector3(Vector3.Zero);
        Direction = new Vector3(Vector3.Zero);
        VerticalMomentum = 0f;
    }

    public override void _Input(InputEvent @event)
    {
        if (@event is InputEventMouseMotion eventMouseMotion)
        {
            // Horizontal rotation.
            head.RotateY(DegToRad(-eventMouseMotion.Relative.x * MouseSensitivity));

            // Vertical rotation.
            float change = -eventMouseMotion.Relative.y * MouseSensitivity;

            if ((change + _cameraAngle < 90) && (change + _cameraAngle > -90))
            {
                camera.RotateZ(DegToRad(change));
                _cameraAngle += change;
            }
        }
    }
    
    public override void _PhysicsProcess(float delta)
    {
        // Camera direction face.
        Basis aim = camera.GetGlobalTransform().basis;

        // Character movement.
        Vector3 target;

        // Reset direction vector.
        Direction = new Vector3();

        // Directional programming.
        if (Input.IsActionPressed("move_forward"))
        {
            Direction -= aim.z;
        }
        if (Input.IsActionPressed("move_backward"))
        {
            Direction += aim.z;
        }
        if (Input.IsActionPressed("move_left"))
        {
            Direction -= aim.x;
        }
        if (Input.IsActionPressed("move_right"))
        {
            Direction += aim.x;
        }

        Direction = Direction.Normalized();

        target = new Vector3(Direction.x, GetVerticalComponent(delta), Direction.z) * _flySpeed;
        Velocity = Velocity.LinearInterpolate(target, _flyAccel * delta);

        MoveAndSlide(Velocity, new Vector3(0, 1, 0));
    }

    // One of the godot methods that gets called every frame.
    public override void _Process(float delta)
    {
        if (Input.IsActionJustPressed("flip_mode"))
        {
            ToggleFly();
        }
    }

    private float DegToRad(float angle) => (float)Math.PI * angle / 180.0f;
    
    private float GetVerticalComponent(float delta)
    {
        if (IsFlying)
        {
            VerticalMomentum = 0;
            return Input.IsActionPressed("move_jump") ? 5 : 0;
        }
        else if (IsOnFloor() && IsFlying)
        {
            VerticalMomentum = 0;
            ToggleFly();

            return VerticalMomentum;
        }
        else
        {
            VerticalMomentum += Input.IsActionPressed("move_jump") && IsOnFloor() 
                ? 20 : _gravity * delta;

            return VerticalMomentum;
        }
    }

    private void ToggleFly() => _isFlying ^= true;
}
