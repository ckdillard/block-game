using Godot;
using System;

public class MainSpatial : Spatial
{
    private PackedScene _player = (PackedScene)GD.Load("res://Scenes/Player.tscn");
    public Node Player { get; private set; }
    public override void _Ready()
    {
        Input.SetMouseMode(Input.MouseMode.Captured);
        Player = _player.Instance();
        AddChild(Player);
    }

    public override void _Process(float delta)
    {
        if (Input.IsActionJustPressed("ui_cancel"))
        {
            GetTree().Quit();
            Input.SetMouseMode(Input.MouseMode.Visible);
        }

        if (Input.IsActionJustPressed("restart"))
        {
            GetTree().ReloadCurrentScene(); 
        }
    }
}
