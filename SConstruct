import os, subprocess

# Constants.
target_path = "BlockGame/bin/"
target_name = ARGUMENTS.get("name", "libgdtest")

godot_headers_path = "godot-cpp/godot_headers/"
cpp_bindings_path = "godot-cpp/"
cpp_library = "godot-cpp/bin/libgodot-cpp"

# Arguments given.
target = ARGUMENTS.get("target", "debug")

platform = ARGUMENTS.get("p", "windows")
platform = ARGUMENTS.get("platform", platform)

env = Environment()
if platform == "windows":
    env = Environment(ENV = os.environ)

if ARGUMENTS.get("use_llvm", "no") == "yes":
    env["CXX"] = "clang++"

# Function to recursively get .cc or .cpp files for compilation.
def add_sources(sources, directory):
    for file in os.listdir(directory):
        if file.endswith(".cc") or file.endswith(".cpp"):
            sources.append(directory + "/" + file)

# Platform-specific config.
if platform == "osx":
    env.Append(CCFLAGS = ["-g", "-O2", "-arch", "x86_64"])
    env.Append(LINKFLAGS = ["-arch", "x86_64"])
    target_path += "osx/"
    cpp_library += f".osx.{target}.64"

if platform == "linux":
    env.Append(CCFLAGS = ["-fPIC", "-g","-O2", "-std=c++14"])
    target_path += "x11/"
    cpp_library += f".linux.{target}.64"

if platform == "windows":
    env.Append(CCFLAGS = ["-DWIN32", "-D_WIN32", "-D_WINDOWS", "-W3", "-GR", "-D_CRT_SECURE_NO_WARNINGS"])
    if target == "debug":
        env.Append(CCFLAGS = ["-EHsc", "-D_DEBUG", "-MDd"])
    else:
        env.Append(CCFLAGS = ["-O2", "-EHsc", "-DNDEBUG", "-MD"])
    target_path += "win64/"
    cpp_library += f".windows.{target}.64"

# Change environment variables.
env.Append(CPPPATH=[".", "src/", godot_headers_path, cpp_bindings_path + "include/", cpp_bindings_path + "include/core/", cpp_bindings_path + "include/gen/"])
env.Append(LIBPATH=[cpp_bindings_path + "bin/"])
env.Append(LIBS=[cpp_library])

sources = []
add_sources(sources, f"src/{target_name}")

library = env.SharedLibrary(target=target_path + target_name, source=sources)
Default(library)